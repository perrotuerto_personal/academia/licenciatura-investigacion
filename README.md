# La fundación de Colima

Aquí se encuentran varios de los materiales relativos a la
investigación de licenciatura, la tesis y el material suplementario
que ha sido publicado después.

## Contenidos

|Nombre | Tipo
--------|----
*La fundación de Colima* | Tesis
*La fundación de Colima: lugar de encuentro y desencuentro de la historiografía regional* | Material suplementario
*Colima y Tuxpan: una historia compartida, una historia en el olvido* | Material suplementario

## Advertencia

Todos los materiales son contenidos antiguos y se les ha dado
poco mantenimiento.

## Licencia

La investigación está bajo [Licencia Editorial Abierta y Libre (LEAL)](https://gitlab.com/prolibreros/legal/leal).
