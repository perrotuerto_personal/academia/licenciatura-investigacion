# *Colima y Tuxpan: una historia compartida, una historia en el olvido*

Aquí se encuentra la ponencia presentada en el
«[VIII Foro Colima y su región](http://www.culturacolima.gob.mx/imagenes/foroscolima/8/VIII-FORO-JUAN-CARLOS-REYES-GARZA.pdf)». 
El evento fue realizado por la Secretaría de Cultura del Estado de Colima
en 2013.
